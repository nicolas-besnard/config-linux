# .bashrc

export EDITOR="emacs -nw"
export PAGER="less"
export BROWSER="google-chrome"

alias ne="emacs -nw"

# ls
alias ls="ls --color"
alias la="ls -A"
alias ll="ls -l"
alias lla="ls -lA"
alias ld="ls -d */"

# shut / restart
alias shu='sudo shutdown -h now'
alias res='sudo shutdown -r now'

alias mv="mv -v"
alias cp="cp -v"
alias rm="rm -v"
alias j="jobs"
alias WWW="cd /var/www/html"
alias go_afs="cd /afs/epitech.net/users/all/besnar_n/"
alias ikoula="ssh cappie@178.170.117.82"
alias update="sudo yum update"
alias install="sudo yum install"
alias remove="sudo yum remove --remove-leaves"
alias clean="find ~/ \( -name '*~' -o -name '#*#' \) -delete -exec echo "Removed" {} \;"

alias svn_epitech="~/.shell/svn_epitech.sh"
alias bitbucket="~/.shell/bitbucket.sh"
alias check_norm="~/.shell/check_norm.sh"
alias zend="~/ZendStudio/ZendStudio &"
alias AFS="sudo /etc/init.d/openafs start"
alias AFS_LOG="klog.krb5"

BLUE="\[\033[0;34m\]"
CYAN="\033[0;36m";
YELLOW="\033[0;33m";
RED="\033[0;31m"
LIGHT_RED="\[\033[1;31m\]"
GREEN="\[\033[0;32m\]"
LIGHT_GREEN="\[\033[1;32m\]"
WHITE="\[\033[1;37m\]"
LIGHT_GRAY="\[\033[0;37m\]"
END_C="\033[0;0m"

function parse_git_branch
{
      RET=$(git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
      if [[ $RET ]]
      then
	  echo -e "${YELLOW}[${END_C}${CYAN}${RET}${END_C}${YELLOW}]${END_C}"
      fi
}

PS1="[\033[31m\u\033[0m] [\A] [\w] \$(parse_git_branch)> "
# MAN
export LESS_TERMCAP_mb=$'\E[0;36m'
export LESS_TERMCAP_md=$'\E[0;36m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[0;44;30m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[1;32m'

#EXTRACT
et () {
    if [ -f $1 ] ; then
	case $1 in
	    *.lrz) lrztar -d $1 && cd $(basename "$1" .lrz) ;;
	    *.tar.bz2) tar xvjf $1 && cd $(basename "$1" .tar.bz2) ;;
	    *.tar.gz) tar xvzf $1 && cd $(basename "$1" .tar.gz) ;;
	    *.tar.xz) tar Jxvf $1 && cd $(basename "$1" .tar.xz) ;;
	    *.bz2) bunzip2 $1 && cd $(basename "$1" /bz2) ;;
	    *.rar) unrar x $1 && cd $(basename "$1" .rar) ;;
	    *.gz) gunzip $1 && cd $(basename "$1" .gz) ;;
	    *.tar) tar xvf $1 && cd $(basename "$1" .tar) ;;
	    *.tbz2) tar xvjf $1 && cd $(basename "$1" .tbz2) ;;
	    *.tgz) tar xvzf $1 && cd $(basename "$1" .tgz) ;;
	    *.zip) unzip $1 && cd $(basename "$1" .zip) ;;
	    *.Z) uncompress $1 && cd $(basename "$1" .Z) ;;
	    *.7z) 7z x $1 && cd $(basename "$1" .7z) ;;
	    *) echo "don't know how to extract '$1'..." ;;
	esac
    else
	echo "'$1' is not a valid file!"
    fi
}



#Prompt KikOo
#PS1='\[\e[0;34m\]┌──< \[\e[1;36m\]\u\[\e[0;34m\] >───< \[\e[0;33m\]\w\[\e[0;34m\] >\n\[\e[0;34m\]└──< \[\e[1;32m\]\A\[\e[0;34m\] >───╼\[\e[0;36m\] '

PROMPT_COMMAND='printf "\033[0;31;0m" "${PWD} "'
#PROMPT_COMMAND='printf "%s" "${PWD/#$HOME/~}"'

repeat()
{
    n=$1
    shift
    while [ $(( n -= 1 )) -ge 0 ]
    do
	"$@"
    done
}

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific aliases and functions
