#!/bin/bash

echo "(1 / 6) Installation libX"
sudo apt-get install libx11-dev
sudo apt-get install libxext-dev
echo "(2 / 6) Extraction"
tar -xf minilibx.tgz
echo "(3 / 6) Compilation"
echo ""
cd minilibx && make
echo ""
echo "(4 / 6) Creation des dossiers"
sudo mkdir -p /usr/X11/include/
sudo mkdir -p /usr/include/X11/
sudo mkdir -p /usr/lib64
echo "(5 / 6) Copie des fichiers"
sudo cp libmlx.a /usr/lib64
sudo cp mlx.h /usr/X11/include/
sudo cp mlx.h /usr/include/X11/
sudo cp mlx.h /usr/include/
echo "(6 / 6) Suppression des fichiers temporaires"
cd ..
rm -rf minilibx
