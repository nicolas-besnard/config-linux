#!/bin/sh

echo "- Emacs"
sudo cp .emacs ~/
sudo cp .myemacs ~/
sudo cp -r .emacs.d/ ~/
echo "- Conky"
sudo cp .conkyrc ~/
echo "- Shell"
sudo cp -r .shell ~/
echo "- BashRC"
sudo cp .bashrc ~/
sudo apt-get update
sudo apt-get install valgrind htop emacs conky
sudo apt-get install compiz compiz-core compiz-gnome compiz-plugins compiz-plugins-default compiz-plugins-extra compiz-plugins-main compiz-plugins-main-default compizconfig-backend-gconf compizconfig-settings-manager libcompizconfig0 libdecoration0 python-compizconfig fusion-icon gnome-tweak-tool
sudu apt-get install git ssh zsh
wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
ssh-keygen

sudo cp xfce4-keyboard-shortcuts.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/
chsh