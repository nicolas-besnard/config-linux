#!/bin/sh
## check_norm.sh for check_norme in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Thu Mar  8 01:34:31 2012 nicolas besnard
## Last update Wed Aug 15 18:06:50 2012 nicolas besnard
##

#~/.shell/norme.py . -malloc -score -nocheat
~/.shell/norme.py . -nocheat -score `find . | grep "\.[ch]$" | sed -e 's/^.\///g' | tr '\n' ' '`