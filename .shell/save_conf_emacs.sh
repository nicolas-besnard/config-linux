#!/bin/sh
## save_conf_emacs.sh for save_conf_emacs in /home/besnar_n//.shell
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Fri Apr  6 21:25:57 2012 nicolas besnard
## Last update Fri Apr 13 22:24:52 2012 nicolas besnard
##

TMP_DIR="/tmp/save_conf/"
SAVE_WWW="/u/all/$USER/www/conf/"

mkdir	-p	${TMP_DIR}
mkdir	-p	${SAVE_WWW}

cp		~/.emacs	${TMP_DIR}
cp		~/.myemacs	${TMP_DIR}
cp	-rf	~/.emacs.d	${TMP_DIR}

tar	-jcf	/tmp/emacs.tar.bz2 ${TMP_DIR}

cp		/tmp/emacs.tar.bz2	${SAVE_WWW}

rm		/tmp/emacs.tar.bz2

rm	-rf	${TMP_DIR}