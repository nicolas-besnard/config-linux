#!/bin/sh
## cpy_lib.sh for cpy_lib in /home/besnar_n//.shell
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Sat Apr 14 12:51:56 2012 nicolas besnard
## Last update Wed Apr 18 09:43:21 2012 besnar_n
##

cp	-rf	~/lib		.
cp	-rf	~/include	.
