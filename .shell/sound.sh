#!/bin/sh
## sound.sh for sound in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Thu Mar  8 00:53:43 2012 nicolas besnard
## Last update Mon Mar 12 17:31:16 2012 nicolas besnard
##


if [ $1 ]
then
    amixer set "Master" $1 | cut -c0- | sed 'd'
else 
    amixer get "Master" | grep '\[.*' | cut -d [ -f 2 | sed '2d' | cut -d ] -f 1
fi;