#!/bin/bash
#
# battery status script
#

function getlevel()
{
    BATTERY=/proc/acpi/battery/BAT0
    REM_CAP=`grep "^remaining capacity" $BATTERY/state | awk '{ print $3 }'`
    FULL_CAP=`grep "^last full capacity" $BATTERY/info | awk '{ print $4 }'`
    BATSTATE=`grep "^charging state" $BATTERY/state | awk '{ print $3 }'`
    CHARGE=`echo $(( $REM_CAP * 100 / $FULL_CAP ))`

    NON='\033[00m'
    BLD='\033[01m'
    RED='\033[01;31m'
    GRN='\033[01;32m'
    YEL='\033[01;33m'

    COLOUR="$RED"
#charged
#charging
#discharging
    if [ "$CHARGE" -gt "99" ]
    then
	CHARGE=100
	COLOUR="$GRN"
    fi

    if [ "$CHARGE" -gt "15" ]
    then
	COLOUR="$YEL"
    fi

    if [ "$CHARGE" -gt "30" ]
    then
	COLOUR="$GRN"
    fi

    if [ "$CHARGE" -le "10" ]
    then
	COLOUR="$RED"
	if [ "${BATSTATE}" != "charging" ]
	then
	    notify-send "Batterie Faible !" "Brancher l'ordinateur à une source de courant." -i /usr/share/gnome-power-manager/icons/hicolor/48x48/status/gpm-ac-adapter.png
	fi
    fi
    echo -e "${COLOUR}${CHARGE}%${NON} ${BATSTATE} ${BATSTT}"

}

function positive_int() { return $(test "$@" -eq "$@" > /dev/null 2>&1 && test "$@" -ge 0 > /dev/null 2>&1); }


function sizetw() {
    if [[ $# -eq 2 ]] && $(positive_int "$1") && $(positive_int "$2"); then
	printf "\e[8;${1};${2};t"
	return 0
    fi
    return 1
}

#sizetw 3 5

while true ;
do
    getlevel
    sleep 300
    clear
done


# end of file
