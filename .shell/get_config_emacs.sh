#!/bin/sh
## get_config_emacs.sh for get_config_emacs in /home/besnar_n//.shell
##
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
##
## Started on  Tue Apr 10 22:08:10 2012 nicolas besnard
## Last update Fri Apr 13 22:25:30 2012 nicolas besnard
##

wget	-P	~/		http://perso.epitech.eu/~besnar_n/conf/emacs.tar.bz2
tar	-jxf	~/emacs.tar.bz2
rm		~/emacs.tar.bz2
cp	-rf	tmp/save_conf/.*emacs*	~/
rm	-rf	tmp/