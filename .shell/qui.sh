
USERS="besnar_n|Nicolas
ettahi_s|Sami
daudet_v|Vincent
bidaul_j|Jeremy
hello_f|Flavien
mercie_j|Joachim
pouly_m|Maximilien
"

NB=0
SHOWOFF=1
QUI_OPT="oh"

### Colors :
C_ESC="\033["
C_END="m"
RED=$C_ESC"31"$C_END
GREEN=$C_ESC"32"$C_END
YELLOW=$C_ESC"33"$C_END
CYAN=$C_ESC"36"$C_END
GREY=$C_ESC"37"$C_END
WHITE=$C_ESC"39"$C_END
DEFAULT=$C_ESC"0"$C_END
###

get_infos ()
{
    LOGIN=$(expr "$USER" : '\(.*\)|')
    PSEUDO=$(expr "$USER" : '.*|\(.*\)')
    NS_WHOz=`ns_who -hs $LOGIN | sed 's/ /#/g'`
    if [ "$NS_WHOz" ]
    then
	AL=0
	for NS_WHOy in $NS_WHOz
	do
	    MACHINE=$(expr "$NS_WHOy" : '\(..........................\).*')
	    MACHINE=$(expr "$MACHINE" : '.*@\(.*\)')
	    MACHINE=`echo -n "$MACHINE" | sed 's/#/ /g'`
	    NS_WHOy=$(expr "$NS_WHOy" : '..........................\(.*\)')
	    PLACE=$(expr "$NS_WHOy" : '\([~:\.a-zA-Z0-9_-]*\)#.*')
	    NS_WHOy=$(expr "$NS_WHOy" : $PLACE'#*\(.*\)')
	    if [ `printf "$PLACE" | grep "epitech.net"` ]
	    then
		PLACE=$(expr "$PLACE" : '\([a-z]*\).*')
	    fi
	    STATUS=$(expr "$NS_WHOy" : '\([a-z]*\).*')
	    COLOR=$WHITE
	    if [ "$STATUS" = "actif" ]
	    then
		COLOR=$GREEN
	    elif [ "$STATUS" = "connection" ]
	    then
		COLOR=$GREEN
	    elif [ "$STATUS" = "away" ]
	    then
		COLOR=$YELLOW
	    elif [ "$STATUS" = "idle" ]
	    then
		COLOR=$YELLOW
	    elif [ "$STATUS" = "lock" ]
	    then
		COLOR=$RED
	    fi
	    TIME=$(expr "$NS_WHOy" : $STATUS'#*\([0-9hm]*\)')
	    print_info
	    AL=$(($AL + 1))
	    NB=$(($NB + 1))
	done
    else
	print_info
    fi
}

print_info ()
{
    if [ "$NS_WHOz" ]
    then
	if [ $AL -eq 0 ]
	then
 	    printf $GREY"+"
	    printf $GREEN"%12.11s  "$ $PSEUDO
	    printf $DEFAULT"%-9s " $LOGIN
	else
	    printf "% 25s"
	fi
	MACHINE=`printf "%.17s" $MACHINE`
	printf $CYAN"%-17s " $MACHINE
	if [ "$STATUS" = "connection" ]
	then
	    if [ `strlen $PLACE` -gt 16 ]
	    then
		printf $GREY"%.15s...  " $PLACE
	    else
		printf $DEFAULT"%-17s  " $PLACE
	    fi
	else
	    if [ `strlen $PLACE` -gt 21 ]
	    then
		printf $GREY"%.18s...  " $PLACE
	    else
		printf $DEFAULT"%-22s " $PLACE
	    fi
	fi
	printf $COLOR"%-6s " $STATUS
	printf $DEFAULT"$TIME\n"
    elif [ $SHOWOFF -eq 1 ]
    then
	printf $RED"%13s "$DEFAULT $PSEUDO
	echo " $LOGIN"
    fi
}

strlen ()
{
    echo $1 | wc -c
}

usage()
{
    echo "Usage : qui [-$QUI_OPT]"
    printf "\t-o\t\tShow offline contacts\n"
    printf "\t-h\t\tShow this message\n"
    exit 0
}

while getopts $QUI_OPT option
do
    case $option in
	o) SHOWOFF=1	;;
	h) usage	;;
	*) exit 1	;;
    esac
done

for USER in $USERS
do
    get_infos
done
