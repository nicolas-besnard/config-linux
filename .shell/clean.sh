#!/bin/sh
## clean.sh for clean in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Thu Mar  8 01:18:40 2012 nicolas besnard
## Last update Thu Mar  8 01:20:06 2012 nicolas besnard
##

find . \( -name '*~' -o -name '#*#' \) -delete -exec echo "Removed" {} \;