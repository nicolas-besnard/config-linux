#!/bin/sh

if [ -z $1 ]; then
    read -p 'Repo Name ? ' repo
    if [ -n $repo ]; then
	git clone git@bitbucket.org:cappie013/$1.git
    else
	echo "Exit."
    fi	
else
    git clone git@bitbucket.org:cappie013/$1.git
fi
