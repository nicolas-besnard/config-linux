#!/bin/bash
## check_battery.sh for check_battery in /home/besnar_n//.shell
##
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
##
## Started on  Wed Apr  4 23:33:24 2012 nicolas besnard
## Last update Thu Apr  5 09:21:52 2012 nicolas besnard
##

BATTERY=/proc/acpi/battery/BAT0
REM_CAP=`grep "^remaining capacity" $BATTERY/state | awk '{ print $3 }'`
FULL_CAP=`grep "^last full capacity" $BATTERY/info | awk '{ print $4 }'`
BATSTATE=`grep "^charging state" $BATTERY/state | awk '{ print $3 }'`
CHARGE=`echo $(( $REM_CAP * 100 / $FULL_CAP ))`
if [ "$CHARGE" -le "10" ]
then
    if [ "${BATSTATE}" != "charging" ]
    then
	notify-send "Batterie Faible (<= 10 %) !" "Brancher l'ordinateur à une source de courant." -i /usr/share/gnome-power-manager/icons/hicolor/48x48/status/gpm-ac-adapter.png
    fi
fi

