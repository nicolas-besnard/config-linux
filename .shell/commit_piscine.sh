#!/bin/sh
## commit_piscine.sh for commit_piscine in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Thu Mar  8 01:29:02 2012 nicolas besnard
## Last update Tue Mar 20 01:35:15 2012 nicolas besnard
##

echo -n "Nom du dossier sur l'AFS dans rendu/piscine ? "
read jour
if [ ! -e /u/all/$USER/rendu/piscine/$jour ]; then
    echo "-> Creation du dossier $jour"
    mkdir -v /u/all/$USER/rendu/piscine/$jour
fi;

cp -rv ~/piscine/$jour/* /u/all/$USER/rendu/piscine/$jour