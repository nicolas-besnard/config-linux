#!/bin/sh
## sh_in_txt.sh for sh_in_txt in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Thu Mar  8 01:12:03 2012 nicolas besnard
## Last update Thu Mar  8 01:12:31 2012 nicolas besnard
##

find . -name "*.sh" -exec rename .sh .txt {} \;