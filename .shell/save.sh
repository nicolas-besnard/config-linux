#!/bin/sh
## save.sh for save in /home/besnar_n//.shell
##
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
##
## Started on  Mon Mar 26 17:16:35 2012 nicolas besnard
## Last update Wed Apr 11 12:46:27 2012 nicolas besnard
##

NOW=$(date +"%d-%m-%Y--%H-%M-%S")

if [ -r $1 ]; then
    mkdir -v ${1}-${NOW}
    cp -v ${1} ${1}-${NOW}
else
    mkdir -v ${1}-${NOW}
    cp -rfv *.* ${1}-${NOW}
fi;