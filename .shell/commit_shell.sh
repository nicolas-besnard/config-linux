#!/bin/sh
## commit_shell.sh for commit_shell in /home/besnar_n//.shell
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Tue Mar 20 00:44:57 2012 nicolas besnard
## Last update Wed Apr  4 21:30:57 2012 nicolas besnard
##

clean

## Supprime les fichiers
rm -v /u/all/$USER/www/shell/*

## Copie des fichiers sur le WWW
cp -v ~/.shell/* /u/all/$USER/www/shell/

## Renome les .sh en .txt (facilite la lecteur)
find /u/all/$USER/www/shell/*.sh -exec rename .sh .txt {} \;