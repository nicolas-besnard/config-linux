#!/bin/sh
## ip.sh for ip in /home/besnar_n//.shell
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Tue Mar 20 01:25:23 2012 nicolas besnard
## Last update Tue Mar 20 01:26:03 2012 nicolas besnard
##

##
## SCRIPT FIND ON INTERNET !
##

/sbin/ifconfig |grep -B1 "inet addr" |awk '{ if ( $1 == "inet" ) { print $2 } else if ( $2 == "Link" ) { printf "%s:" ,$1 } }' |awk -F: '{ print $1 ": " $3 }'

lynx --dump http://whatismyip.org