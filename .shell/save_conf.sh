#!/bin/sh
## save_conf.sh for save_conf in /home/besnar_n//.shell
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Tue Mar 20 00:36:04 2012 nicolas besnard
## Last update Wed Mar 21 18:13:32 2012 nicolas besnard
##

function exist
{
    if [ -e $1 ]; then
	echo "[1]"
    else
	echo "[0]" 
    fi;   
}


NOW=$(date +"%d-%m-%Y--%H-%M-%S")
CONF="conf.$NOW"
TMP_DIR="/tmp/save_conf/"
CONF_DIR="/u/all/$USER/public/conf/"

## Creation du dossier de sauvegarde sur l'AFS
if [ ! -e $CONF_DIR/$CONF ]; then
    echo "Creation du dossier de sauvegarde dans $CONF_DIR"
    mkdir -p $CONF_DIR/$CONF
fi;

## Creation d'un dossier temporaire
echo -n "Creation du dossier temporaire : "
mkdir $TMP_DIR
exist $TMP_DIR

## Creation des archives
echo "Création des archives :"

echo -n "- Shell : "
tar -jcf $TMP_DIR/shell.tar.bz2 ~/.shell/
exist $TMP_DIR/shell.tar.bz2

echo -n "- Alias : "
tar -jcf $TMP_DIR/alias.tar.bz2 ~/.tcshrc
exist $TMP_DIR/alias.tar.bz2

echo -n "- Conky : "
tar -jcf $TMP_DIR/conky.tar.bz2 ~/.conkyrc
exist $TMP_DIR/conky.tar.bz2

echo -n "- Emacs : "
tar -jcf $TMP_DIR/emacs.tar.bz2 ~/.emacs ~/.myemacs ~/.myemacs/
exist $TMP_DIR/emacs.tar.bz2

echo -n "- Fluxbox :"
tar -jcf $TMP_DIR/fluxbox.tar.bz2 ~/.fluxbox/
exist $TMP_DIR/fluxbox.tar.bz2

## Upload des fichiers
echo "Upload des fichiers :"

echo -n "- Shell : "
cp $TMP_DIR/shell.tar.bz2 $CONF_DIR/$CONF
exist $TMP_DIR/shell.tar.bz2

echo -n "- Alias : "
cp $TMP_DIR/alias.tar.bz2 $CONF_DIR/$CONF
exist $TMP_DIR/alias.tar.bz2

echo -n "- Conky : "
cp $TMP_DIR/conky.tar.bz2 $CONF_DIR/$CONF
exist $TMP_DIR/conky.tar.bz2

echo -n "- Emacs : "
cp $TMP_DIR/emacs.tar.bz2 $CONF_DIR/$CONF
exist $TMP_DIR/emacs.tar.bz2

echo -n "- Fluxbox :"
cp $TMP_DIR/fluxbox.tar.bz2 $CONF_DIR/$CONF
exist $TMP_DIR/fluxbox.tar.bz2

## Suppression du dossier temporaire
rm -rf $TMP_DIR