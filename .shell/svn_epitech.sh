#!/bin/sh
## svn_epitech.sh for svn in /home/besnar_n
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Sun Aug 12 18:49:49 2012 nicolas besnard
## Last update Thu Aug 16 13:57:42 2012 nicolas besnard
##

if [ -z $1 ]; then
    read -p 'Repo Name ? ' repo
    if [ -n $repo ]; then
	svn co svn+ssh://kscm@koala-rendus.epitech.net/$repo
    else
	echo "Exit."
    fi	
else
    svn co svn+ssh://kscm@koala-rendus.epitech.net/$1
fi