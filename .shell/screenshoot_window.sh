#!/bin/sh
## screenshoot_window.sh for screenshoot_window in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Sat Mar 10 00:35:51 2012 nicolas besnard
## Last update Sat Mar 10 00:36:54 2012 nicolas besnard
##
PRTSCR="$HOME/Images"

DATE=`date +%d%b%Y-%H:%M:%S`

if [ ! -d ${PRTSCR} ]
then
    mkdir -p ${PRTSCR}
fi

import -frame "$PRTSCR"/"$DATE".ps