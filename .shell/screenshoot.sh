#!/bin/sh
## screenshoot.sh for screenshoot in /home/besnar_n/
## 
## Made by nicolas besnard
## Login   <besnar_n@epitech.net>
## 
## Started on  Sat Mar 10 00:21:20 2012 nicolas besnard
## Last update Sat Mar 10 00:34:35 2012 nicolas besnard
##

PRTSCR="$HOME/Images"

DATE=`date +%d%b%Y-%H:%M:%S`

if [ ! -d ${PRTSCR} ]
then
    mkdir -p ${PRTSCR}
fi

import -window root "$PRTSCR"/"$DATE".ps