;;
;; skeleton.el for skeleton in /home/besnar_n/.emacs.d
;; 
;; Made by nicolas besnard
;; Login   <besnar_n@epitech.net>
;; 
;; Started on  Fri Apr 13 21:41:14 2012 
;; Last update Wed Sep 26 11:57:13 2012 nicolas besnard
;;

;; Skeleton des headers protege
(define-skeleton insert-protect-header
  "Inserts a define to protect the header file."
  ""
  '(setq str (file-name-sans-extension
	      (file-name-nondirectory (buffer-file-name))))
  "#ifndef "(upcase str)"_H_\n"
  "# define "(upcase str)"_H_\n"
  "\n"
  "\n"
  "\n"
  "#endif /* !"(upcase str)"_H_ */\n"
)

;; Skeleton des Makefiles
(define-skeleton create-makefile
  "Create a Makefile."
  ""
  "BOLD		=	\\033[1m\n"
  "COMPIL_COLOR	=	\\033[33m\n"
  "LINK_COLOR	=	\\033[32m\n"
  "CLEAN_COLOR	=	\\033[31m\n"
  "END		=	\\033[0m\n\n"
  "NAME		=	\n\n"
  "CC		=	gcc\n\n"
  "RM		=	rm -f\n\n"
  "CFLAGS	\t=	-Wextra -Wall -Werror \\\n"
  "			-ansi -pedantic -O3 \\\n"
  "			-Ilib/include/ \\\n"
  "			-Iheaders/\n\n"
  "LDFLAGS	\t=	-Llib/ -lmy\n"
  "#LDFLAGS	+=	-L/usr/lib64 -l mlx -L/usr/lib64/X11 -lXext -lX11\n"
  "\n"
  "SRCS		=	$(wildcard *.c) \n"
  "\n"
  "OBJS		=	$(SRCS:.c=.o)\n"
  "\n"
  "ECHO		=	echo -e\n"
  "\n"
  "all		:	$(NAME)\n"
  "\n"
  "$(NAME)	\t:	$(OBJS)\n"
  "			@$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)\n"
  "			@$(ECHO) '$(LINK_COLOR)# Creation de $(BOLD)$@$(END)'\n"
  "\n"
  "%.o: %.c\n"
  "			@$(CC) -o $@ -c $< $(CFLAGS)\n"
  "			@$(ECHO) '$(COMPIL_COLOR)+ Compilation de $(BOLD)$@$(END)'\n"
  "clean	\t:\n"
  "			-@$(RM) $(OBJS)\n"
  "			@$(ECHO) '$(CLEAN_COLOR)+ Directory cleaned$(END)'\n"
  "\n"
  "fclean	\t:	clean\n"
  "			-@$(RM) $(NAME)\n"
  "			@$(ECHO) '$(CLEAN_COLOR)+ Removed $(BOLD)$(NAME)$(END)'"
  "\n"
  "re		:	fclean all\n"
  "\n"
  ".PHONY	\t:	all clean re\n"
)